import traceback
import logging
import json
import pymysql
import uuid
import sys
import os

import time

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_CreatePresItemID():
    """
    Name - fn_createTherapistSessionID

    Description - Returns a unique CutitronicsSessionID in correct format

    Arguments - N/A

    Returns:
        - String: CutitronicsSessionID Value
    """

    return str(uuid.uuid4())


def fn_CreateTherapistSession(lv_CutitronicsSessionID, lv_CutitronicsTherapistID, lv_CutitronicsClientID, lv_SessionTypeCode, lv_Source):
    """
    Name - fn_createTherapistSession

    Description - Creates a therapist session object within the database

    Arguments:
        lv_CutitronicsSessionID (String)
        lv_CutitronicsTherapistID (String)
        lv_CutitronicsClientID (String)
        lv_SessionTypeCode (String)
        lv_Source (String)

    Returns:
        300 - Failure, unable to create session object in TherapistSession
        100 - Success, A new Therapist Session object has been created in the back office
    """
    lv_statement = "INSERT INTO TherapistSession (CutitronicsSessionID, CutitronicsTherapistID, CutitronicsClientID, SessionTypeCode, SessionStatus, SessionStart, SessionEnd, CurrentTemperature, CreationDate, CreationUser, LastUpdateDate, LastUpdateUser, Attribute1, Attribute2, Attribute3) VALUES (%(CutitronicsSessionID)s, %(CutitronicsTherapistID)s, %(CutitronicsClientID)s, %(SessionTypeCode)s, 'Open', SYSDATE(), NULL, NULL, SYSDATE(), %(CreationUser)s, SYSDATE(), NULL, NULL, NULL, NULL)"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsSessionID': lv_CutitronicsSessionID, 'CutitronicsTherapistID': lv_CutitronicsTherapistID, 'CutitronicsClientID': lv_CutitronicsClientID, 'SessionTypeCode': lv_SessionTypeCode, 'CreationUser': lv_Source})
            if cursor.rowcount == 1:
                return(100)
    except pymysql.err.IntegrityError as e:
        logger.info(e.args)
        return 300


def fn_CheckPrescriptionExists(lv_CutitronicsPresID):

    lv_list = ""
    lv_Stmt = "SELECT CutitronicsPresID, PrescriptionName FROM BrandPrescriptions WHERE CutitronicsPresID = %(CutitronicsPresID)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_Stmt, {'CutitronicsPresID': lv_CutitronicsPresID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if cursor.rowcount == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def fn_CreatePrescriptionItem(lv_CutitronicsPresHDRID, lv_CutitronicsPresDETID, lv_CutitronicsSKUCode, lv_PrescriptionType, lv_Frequency, lv_FrequencyType, lv_UsageAM, lv_UsagePM, lv_RecommendedAmount, lv_CreationUser):

    lv_list = ""
    lv_stmt = "INSERT INTO BrandPrescriptionItems (CutitronicsPresHDRID, CutitronicsPresDETID, CutitronicsSKUCode, PrescriptionType, Frequency, Frequencytype, UsageAM, UsagePM, RecommendedAmount, CreationDate, CreationUser, LastUpdateDate, LastUpdateUser, Attribute1, Attribute2, Attribute3) VALUES (%(CutitronicsPresHDRID)s, %(CutitronicsPresDETID)s, %(CutitronicsSKUCode)s, %(PrescriptionType)s, %(Frequency)s, %(FrequencyType)s, %(UsageAM)s, %(UsagePM)s, %(RecommendedAmount)s, SYSDATE(), %(CreationUser)s, SYSDATE(), %(CreationUser)s, NULL, NULL, NULL)"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_stmt, {'CutitronicsPresHDRID': lv_CutitronicsPresHDRID, 'CutitronicsPresDETID': lv_CutitronicsPresDETID, 'CutitronicsSKUCode': lv_CutitronicsSKUCode, 'PrescriptionType': lv_PrescriptionType, 'Frequency': lv_Frequency, 'FrequencyType': lv_FrequencyType, 'UsageAM': lv_UsageAM, 'UsagePM': lv_UsagePM, 'RecommendedAmount': lv_RecommendedAmount, 'CreationUser': lv_CreationUser})
            if cursor.rowcount == 1:
                return(200)
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500
        
    


def lambda_handler(event, context):
    """
    createSession

    Creates a new Therapist Session
    """
    openConnection()

    logger.info("Lambda function - {function_name}".format(function_name=context.function_name))

    # Variables

    lv_msgVersion = None
    lv_testFlag = None
    lv_PresItemID = None
    lv_CutitronicsPresID = None
    lv_CutitronicsSKUCode = None
    lv_CutitronicsClientID = None
    lv_SessionTypeCode = None
    lv_Source = None

    # Return block

    CreatePresItem_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {"CutitronicsPresHDRID": lv_PresItemID, "CutitronicsPresDetID": lv_CutitronicsPresID, "CutitronicsSKUCode": lv_CutitronicsSKUCode}}

    try:
        try:
            body = json.loads(event['body'])
        except KeyError as e:
            try:
                body = event
            except TypeError:
                logger.error("The payload is not in the right format")
                app_json = json.dumps(event)
                event = {"body": app_json}
                body = json.loads(event['body'])

        logger.info("Payload body - '{body}'".format(body=body))
        logger.info(" ")
        logger.info("Payload event - '{lv_event}'".format(lv_event=event))

        lv_msgVersion = body.get('msgVersion', 0)
        lv_testFlag = body.get('testFlag', 0)

        # DB Key vars
        lv_CutitronicsPresID = body.get('CutitronicsPresID', 0)
        lv_CutitronicsSKUCode = body.get('CutitronicsSKUCode', "")

        # Usage vars
        lv_PrescriptionType = body.get('PrescriptionType', "0")
        lv_Frequency = body.get('Frequency', "")
        lv_FrequencyType = body.get('FrequencyType', '')
        lv_UsageAM = body.get("UsageAM", 0)
        lv_UsagePM = body.get("UsagePM", 0)
        lv_RecommendedAmount = body.get("RecommendedAmount", "")

        # old vars
        lv_Source = body.get("Source", "createPrescriptionItem")

        '''
        1. Does Brand exist
        '''

        logger.info(" ")
        logger.info("Calling fn_CheckPrescriptionExists to check if the following prescription exists - %s", lv_CutitronicsPresID)
        logger.info(" ")

        lv_Prescription = fn_CheckPrescriptionExists(lv_CutitronicsPresID)

        logger.info(" ")
        logger.info("fn_CheckPrescriptionExists return - %s", lv_Prescription)
        logger.info(" ")

        if lv_Prescription == 500 or lv_Prescription == 400:  # Failure

            logger.error("Prescription '{lv_CutitronicsPresID}' has not been found in the back office systems".format(lv_CutitronicsPresID=lv_CutitronicsPresID))

            CreatePresItem_out['Status'] = "Error"
            CreatePresItem_out['ErrorCode'] = context.function_name + "_001"  # CreateTherapistSession_001
            CreatePresItem_out['ErrorDesc'] = "Supplied prescriptionID does not exist in the BO Database"
            CreatePresItem_out["ServiceOutput"]["CutitronicsPresHDRID"] = body.get('CutitronicsPresID', "")
            CreatePresItem_out["ServiceOutput"]["CutitronicsSKUCode"] = lv_CutitronicsSKUCode

            # Lambda response
            Connection.close()
            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(CreatePresItem_out)
            }

        '''
        7. Get Session ID
        '''
        lv_PresItemID = fn_CreatePresItemID()

        logger.info(" ")
        logger.info("Generating Session code - %s", lv_PresItemID)
        logger.info(" ")

        '''
        8. Create Therapist Session
        '''
        lv_CreatePresItem = fn_CreatePrescriptionItem(lv_CutitronicsPresID, lv_PresItemID, lv_CutitronicsSKUCode, lv_PrescriptionType, lv_Frequency, lv_FrequencyType, lv_UsageAM, lv_UsagePM, lv_RecommendedAmount, lv_Source)

        if lv_CreatePresItem == 500:  # Failure

            logger.error("Prescription Item '{lv_Session}' has not been created in the back office systems".format(lv_Session=lv_PresItemID))

            CreatePresItem_out['Status'] = "Error"
            CreatePresItem_out['ErrorCode'] = context.function_name + "_005"  # CreateTherapistSession_005
            CreatePresItem_out['ErrorDesc'] = "Couldn't create seprescription item in database"
            CreatePresItem_out["ServiceOutput"]["CutitronicsPresDetID"] = lv_PresItemID
            CreatePresItem_out["ServiceOutput"]["CutitronicsPresHDRID"] = body.get('CutitronicsPresID', "")
            CreatePresItem_out["ServiceOutput"]["CutitronicsSKUCode"] = lv_CutitronicsSKUCode

            Connection.close()
            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(CreatePresItem_out)
            }

        # Create output

        Connection.commit()
        Connection.close()

        logger.info("Created Session with CutitronicsSessionID: %s", lv_PresItemID)

        CreatePresItem_out['Status'] = "Success"
        CreatePresItem_out['ErrorCode'] = ""
        CreatePresItem_out['ErrorDesc'] = ""
        CreatePresItem_out["ServiceOutput"]["CutitronicsPresDetID"] = lv_PresItemID
        CreatePresItem_out["ServiceOutput"]["CutitronicsPresHDRID"] = body.get('CutitronicsPresID', "")
        CreatePresItem_out["ServiceOutput"]["CutitronicsSKUCode"] = lv_CutitronicsSKUCode

        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(CreatePresItem_out)
        }

    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)

        Connection.rollback()
        Connection.close()

        # Populate Cutitronics reply block

        CreatePresItem_out['Status'] = "Error"
        CreatePresItem_out['ErrorCode'] = context.function_name + "_000"  # CreateTherapistSession_000
        CreatePresItem_out['ErrorDesc'] = "CatchALL"
        CreatePresItem_out["ServiceOutput"]["CutitronicsSessionID"] = 0
        CreatePresItem_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsPresID
        CreatePresItem_out["ServiceOutput"]["CutitronicsTherapistID"] = lv_CutitronicsSKUCode
        CreatePresItem_out["ServiceOutput"]["CutitronicsClientID"] = body.get('CutitronicsClientID', "")

        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(CreatePresItem_out)
        }
